psql postgres postgres
1234
create database cotisse;
create user ravaka with password 'ravaka';
grant all privileges on database "cotisse" to ravaka;
\q
psql cotisse ravaka
ravaka

create sequence seq_utilisateur;

create table utilisateur(
	id varchar(50) primary key not null,
	nom varchar(50) not null,
	mdp varchar(100) not null,
	tel varchar(10) not null,
	type varchar(20) not null
);

create sequence seq_password;
create table password(
    id varchar(50) primary key not null,
    idUser varchar(50) references utilisateur(id),
    mdp varchar(75) not null,
    datePassword timestamp not null
);

create sequence seq_vehicule;

create table vehicule(
	id varchar(50) primary key,
	marque varchar(50) not null,
	nbPlace integer not null,
	dateDebutService date,
	etat integer not null
);

create sequence seq_place;

create table place(
	id varchar(50) primary key,
	idVehicule varchar(50) references vehicule(id),
	rang integer not null,
	etat integer not null
);

create sequence seq_lieu;

create table lieu(
	id varchar(50) primary key,
	description varchar(50) not null
);

create sequence seq_infoDeplacement;

create table infoDeplacement(
        id varchar(50) primary key,
	idLieu1 varchar(50) references lieu(id),
	idLieu2 varchar(50) references lieu(id),
	montant decimal not null,
	distance decimal not null,
	dateApplication date not null
);

create sequence seq_voyage;

create table voyagePlanifie(

);

create table voyage(
	id varchar(50) primary key,
	depart varchar(50) references lieu(id),
	arrive varchar(50) references lieu(id),
	dateVoyage timestamp not null,
	idVehicule varchar(50) references vehicule(id),
	delaiPaiement integer not null,
    etat integer not null
);
create table voyageAnnule(
    id varchar(50) references voyage(id) primary key,
    dateAnnulation timestamp not null
);
create table voyageConfirme(
    id varchar(50) references voyage(id) primary key,
    dateConfirmation timestamp not null
);

create table arriveVoyage(
    id varchar(50) references voyage(id) primary key,
    dateArrive timestamp not null
);

create table confirmationArriveVoyage(
    id varchar(50) references arriveVoyage(id),
    HeureArriveReel timestamp not null,
    dateCOnfirmation timestamp not null
);

--voyage en cours
create view voyageValide as select voyage.* from voyage where id not in
(select id from voyageAnnule) and id not in (select id from arriveVoyage);

create view voyageEnCours as select voyage.* from voyage where id not in (select id from voyageAnnule) and id not in (select id from arriveVoyage) and etat=11;

create view voyageTermine as
select voyage.id as idVoyage, vehicule.id as idVehicule, depart as lieu, dateAnnulation as date
from voyageAnnule
join voyage
on voyageAnnule.id = voyage.id
join vehicule
on voyage.idVehicule = vehicule.id
union
select voyage.id as idVoyage, vehicule.id as idVehicule, arrive as lieu, dateArrive as date
from arriveVoyage
join voyage
on arriveVoyage.id = voyage.id
join vehicule
on voyage.idVehicule = vehicule.id;

create sequence seq_reservation;

create table reservation(
	id varchar(50) primary key,
	idClient varchar(50) references utilisateur(id),
	idVoyage varchar(50) references voyage(id),
	dateReservation timestamp not null,
	nbPlace integer not null,
	etat integer not null
);

create sequence seq_reservationdetail;
create table reservationDetail(
	id varchar(50) primary key,
	idReservation varchar(50) references reservation(id),
	idPlace varchar(50) references place(id)
);

create sequence seq_types;
create table types(
	id varchar(20) primary key,
	descript varchar(50) not null,
	multiplicateur Decimal not null
);

create table typeVehicule(
	idVehicule varchar(50) references vehicule(id),
	idType varchar(50) references types(id),
	dateApplication timestamp  not null
);

create sequence seq_paiement;

create table paiement(
	id varchar(50) primary key,
	idReservation varchar(50) references reservation(id),
	montant decimal not null
);

create sequence seq_options;

create table options(
	id varchar(20) primary key,
	descript varchar(50),
	prix Decimal
);

create table optionReservation(
	idReservationDetail varchar(50) references reservationDetail(id),
	idOption varchar(50) references options(id)
);

create sequence seq_maintenance;

create table maintenance(
	id varchar(50) primary key,
	idVehicule varchar(50),
	montantMaintenance Decimal,
	consommation Decimal,--PAR KILOMETRE
	dateApplication date
);

create sequence seq_token;
create table token(
    id varchar(20) primary key not null,
    token varchar(50) not null,
    idUtilisateur varchar(50) references utilisateur(id),
    dateCreation timestamp not null,
    dateExpiration timestamp,
    dateFin timestamp,
    etat int not null
);

create view voyageComplet as 
select Voyage.*,MAX(CASE lieu.id WHEN voyage.depart THEN lieu.description end) as departdesc,MAX(CASE lieu.id WHEN voyage.arrive THEN lieu.description end) as arrivedesc,vehicule.marque,nbPlace, types.descript,types.multiplicateur,montant,distance from voyage 
join vehicule on vehicule.id=voyage.idVehicule 
join typeVehicule on typeVehicule.idVehicule=vehicule.id 
join types on types.id=typevehicule.idType 
join lieu on lieu.id=voyage.depart or lieu.id=voyage.arrive
join infoDeplacement on (infoDeplacement.idlieu1=voyage.depart OR infoDeplacement.idlieu1=voyage.arrive) AND (infoDeplacement.idlieu2=voyage.depart OR infoDeplacement.idlieu2 = voyage.arrive )
group by Voyage.id,types.id,vehicule.id,infoDeplacement.id,infoDeplacement.id;

create view PrixReservationDetail as
select ReservationDetail.id,ReservationDetail.idReservation,ReservationDetail.idReservation,max(montant*multiplicateur) as montanttotal,sum(options.prix) as montantoptions
from reservationdetail
JOIN reservation on reservation.id=reservationDetail.idReservation
JOIN voyageComplet on voyageComplet.id=Reservation.idVoyage
JOIN optionReservation on optionReservation.idReservationDetail=reservationDetail.id
JOIN options on options.id=optionReservation.idOption
group by ReservationDetail.id,options.id;

--DONNEES
insert into utilisateur values('util' || nextval('seq_utilisateur'), 'Ravaka', md5('ravaka'), '0332456378', 'caissier');
insert into utilisateur values('util' || nextval('seq_utilisateur'), 'Fenitra', md5('fenitra'), '0324162543', 'admin');
insert into utilisateur values('util' || nextval('seq_utilisateur'),'Koto', md5('koto'), '0347634526', 'chauffeur');

insert into vehicule values('vcl' || nextval('seq_vehicule'), 'mazda', '18', '2015-01-12',1);
insert into vehicule values('vcl' || nextval('seq_vehicule'), 'mazda', '18', '2012-03-24',1);
insert into vehicule values('vcl' || nextval('seq_vehicule'), 'mercedes-sprinter', '22', '2016-02-05',1);
insert into vehicule values('vcl' || nextval('seq_vehicule'), 'mercedes-sprinter', '22', '2016-03-01',1);
insert into vehicule values('vcl' || nextval('seq_vehicule'), 'mercedes-sprinter', '22', '2015-10-10',1);

insert into place values('plc' || nextval('seq_place'), 'vcl1', 1, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 2, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 3, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 4, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 5, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 6, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 7, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 8, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 9, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 10, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 11, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 12, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 13, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 14, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 15, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 16, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 17, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl1', 18, 1);

insert into place values('plc' || nextval('seq_place'), 'vcl2', 1, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 2, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 3, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 4, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 5, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 6, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 7, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 8, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 9, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 10, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 11, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 12, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 13, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 14, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 15, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 16, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 17, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl2', 18, 1);

insert into place values('plc' || nextval('seq_place'), 'vcl3', 1, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 2, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 3, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 4, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 5, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 6, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 7, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 8, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 9, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 10, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 11, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 12, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 13, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 14, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 15, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 16, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 17, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 18, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 19, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 20, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 21, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl3', 22, 1);

insert into place values('plc' || nextval('seq_place'), 'vcl4', 1, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 2, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 3, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 4, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 5, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 6, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 7, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 8, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 9, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 10, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 11, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 12, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 13, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 14, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 15, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 16, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 17, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 18, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 19, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 20, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 21, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl4', 22, 1);

insert into place values('plc' || nextval('seq_place'), 'vcl5', 1, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 2, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 3, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 4, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 5, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 6, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 7, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 8, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 9, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 10, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 11, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 12, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 13, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 14, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 15, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 16, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 17, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 18, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 19, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 20, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 21, 1);
insert into place values('plc' || nextval('seq_place'), 'vcl5', 22, 1);

insert into lieu values('L'||nextval('seq_lieu'),'Antsiranana');
insert into lieu values('L'||nextval('seq_lieu'),'Mahajanga');
insert into lieu values('L'||nextval('seq_lieu'),'Toamasina');
insert into lieu values('L'||nextval('seq_lieu'),'Antananarivo');
insert into lieu values('L'||nextval('seq_lieu'),'Antsirabe');
insert into lieu values('L'||nextval('seq_lieu'),'Fianarantsoa');
insert into lieu values('L'||nextval('seq_lieu'),'Ambatondrazaka');
insert into lieu values('L'||nextval('seq_lieu'),'Morondava');
insert into lieu values('L'||nextval('seq_lieu'),'Toliara');
--UTILISATEUR

insert into voyage values('V'||nextval('seq_voyage'),'L4','L3','2020/01/01','vcl1',2,1);

insert into types values('TP'||nextval('seq_types'),'Lite',1);
insert into types values('TP'||nextval('seq_types'),'Premium',2);
insert into types values('TP'||nextval('seq_types'),'VIP',3);

insert into typeVehicule values('vcl1','TP1','2020/01/01');
insert into typeVehicule values('vcl2','TP1','2020/01/01');
insert into typeVehicule values('vcl3','TP2','2020/01/01');
insert into typeVehicule values('vcl4','TP2','2020/01/01');
insert into typeVehicule values('vcl5','TP3','2020/01/01');

insert into infoDeplacement values('INFD'||nextval('seq_infodeplacement'),'L4','L1',40000,200,'2020/01/01');
insert into infoDeplacement values('INFD'||nextval('seq_infodeplacement'),'L4','L2',20000,300,'2020/01/01');
insert into infoDeplacement values('INFD'||nextval('seq_infodeplacement'),'L4','L3',30000,365,'2020/01/01');
insert into infoDeplacement values('INFD'||nextval('seq_infodeplacement'),'L4','L5',30000,250,'2020/01/01');
insert into infoDeplacement values('INFD'||nextval('seq_infodeplacement'),'L4','L6',25000,275,'2020/01/01');
insert into infoDeplacement values('INFD'||nextval('seq_infodeplacement'),'L4','L7',40000,200,'2020/01/01');
insert into infoDeplacement values('INFD'||nextval('seq_infodeplacement'),'L4','L8',15000,150,'2020/01/01');
insert into infoDeplacement values('INFD'||nextval('seq_infodeplacement'),'L4','L9',25000,175,'2020/01/01');

insert into options values ('OPT'||nextval('seq_options'),'Bouteille d eau',1200);
