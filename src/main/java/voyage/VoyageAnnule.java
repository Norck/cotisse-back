package voyage;

import app.HttpResponse;
import dao.Connect;
import dao.OutilsSql;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class VoyageAnnule {
    private String id;
    private LocalDateTime dateAnnulation;

    public void setId(String id) {
        this.id = id;
    }

    public void setDateAnnulation(LocalDateTime dateAnnulation) {
        this.dateAnnulation = dateAnnulation;
    }

    public String getId() {
        return id;
    }

    public LocalDateTime getDateAnnulation() {
        return dateAnnulation;
    }

    public VoyageAnnule(String id, LocalDateTime dateAnnulation) {
        this.id = id;
        this.dateAnnulation = dateAnnulation;
    }

    public HttpResponse annulerVoyage(){
        Connection connection = null;
        OutilsSql outilsSql = new OutilsSql();
        try{
            connection = Connect.connect("src/main/java/dao/postgresql.xml");
            connection.setAutoCommit(false);
            if(connection == null){
                return new HttpResponse(503, "Echec de la connection à la base.", null);
            }
            String[][] result = outilsSql.getResultSql(connection, "select * from voyage where id='" + this.getId() + "'");
            if(result.length == 0){
                return new HttpResponse(204, "Voyage non existant.", null);
            }
            int etat = Integer.parseInt(result[0][6]);
            System.out.println(etat);
            if(etat == 0){
                return new HttpResponse(204, "Voyage déjà annulé.", null);
            }
            if(etat == 21){
                return new HttpResponse(204, "Voyage déjà terminée.", null);
            }

            String query = "update voyage set etat=0 where id='" + this.getId() + "'";
            System.out.println(query);
            outilsSql.execute_query(connection, query);
            outilsSql.insertRow(this, "voyageAnnule");
            connection.commit();
            return new HttpResponse(204, "Annulation voyage reussit.", null);
        }catch(Exception e) {
            return new HttpResponse(204, "Annulation voyage echouée.", null);
        }finally{
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
