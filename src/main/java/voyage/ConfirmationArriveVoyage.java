package voyage;

import app.HttpResponse;
import dao.Connect;
import dao.OutilsSql;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class ConfirmationArriveVoyage {
    private String id;
    private LocalDateTime heureArriveReel;
    private LocalDateTime dateConfirmation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getHeureArriveReel() {
        return heureArriveReel;
    }

    public void setHeureArriveReel(LocalDateTime heureArriveReel) {
        this.heureArriveReel = heureArriveReel;
    }

    public LocalDateTime getDateConfirmation() {
        return dateConfirmation;
    }

    public void setDateConfirmation(LocalDateTime dateConfirmation) {
        this.dateConfirmation = dateConfirmation;
    }

    public ConfirmationArriveVoyage(String id, LocalDateTime heureArriveReel, LocalDateTime dateConfirmation) {
        this.id = id;
        this.heureArriveReel = heureArriveReel;
        this.dateConfirmation = dateConfirmation;
    }

    public HttpResponse confirmerArriveVoyage(){
        Connection connection = null;
        OutilsSql outilsSql = new OutilsSql();
        if(dateConfirmation.isBefore(heureArriveReel)){
            return new HttpResponse(204, "Confirmation avant arrivée réelle.", null);
        }
        try{
            connection = Connect.connect("src/main/java/dao/postgresql.xml");
            connection.setAutoCommit(false);
            if(connection == null){
                return new HttpResponse(503, "Echec de la connection à la base.", null);
            }
            String[][] result = OutilsSql.getResultSql(connection, "select etat from voyage where id='" + this.getId() + "'");
            int etat = Integer.parseInt(result[0][0]);
            if(etat == 0){
                return new HttpResponse(204, "Voyage déjà annulé.", null);
            }
            outilsSql.execute_query(connection, "update voyage set etat=21 where id='" + this.getId() + "'");
            outilsSql.insertRow(connection, this, "confirmationArriveVoyage");
            connection.commit();
            return new HttpResponse(204, "Confirmation arrivée réussite", null);
        }catch(Exception e){
            return new HttpResponse(204, "Echec de la confirmation de l'arrivée.", null);
        }finally{
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
