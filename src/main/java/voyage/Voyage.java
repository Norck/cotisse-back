package voyage;

import app.HttpResponse;
import dao.Connect;
import dao.OutilsSql;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
public class Voyage {

    public void setId(String id) {
        this.id = id;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public void setArrive(String arrive) {
        this.arrive = arrive;
    }

    public void setDateVoyage(LocalDateTime dateVoyage) {
        this.dateVoyage = dateVoyage;
    }

    public void setIdVehicule(String idVehicule) {
        this.idVehicule = idVehicule;
    }

    public void setDelaiPaiement(int delaiPaiement) {
        this.delaiPaiement = delaiPaiement;
    }

    public String getId() {
        return id;
    }

    public String getDepart() {
        return depart;
    }

    public String getArrive() {
        return arrive;
    }

    public LocalDateTime getDateVoyage() {
        return dateVoyage;
    }

    public String getIdVehicule() {
        return idVehicule;
    }

    public int getDelaiPaiement() {
        return delaiPaiement;
    }

    public int getEtat(){ return etat; }
    public void setEtat(int etat){ this.etat = etat; }

    private String id;
    private String depart;
    private String arrive;
    private LocalDateTime dateVoyage;
    private String idVehicule;
    private int delaiPaiement;
    private int etat;


    public Voyage(String id, String depart, String arrive, LocalDateTime dateVoyage, String idVehicule, int delaiPaiement) throws Exception {
        if(depart.contentEquals(arrive)){
            throw new Exception("Depart identique à arrivé.");
        }
        this.id = id;
        this.depart = depart;
        this.arrive = arrive;
        this.dateVoyage = dateVoyage;
        this.idVehicule = idVehicule;
        this.delaiPaiement = delaiPaiement;
    }

    //planifier voyage
    public HttpResponse insertVoyage(){
        Connection connection = null;
        this.etat = 1;
        try{
            connection = Connect.connect("src/main/java/dao/postgresql.xml");
            OutilsSql outilsSql = new OutilsSql();
            if(connection == null){
                return new HttpResponse(503, "Echec de la connection à la base.", null);
            }

            String[][] result = outilsSql.getResultSql(connection,"select date, lieu from voyageTermine where idVehicule='" + this.getIdVehicule() + "'");
            if(!result[0][1].contentEquals(this.getDepart())){
                return new HttpResponse(500, "Lieu de départ different du lieu de disponibilité.", null);
            }
            System.out.println("1");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateDisponibilite = LocalDateTime.parse(result[0][0], formatter);
            if( dateDisponibilite.isAfter(this.getDateVoyage() )) {
                return new HttpResponse(500, "Voyage avant disponibilité voiture.", null);
            }
            System.out.println("2");

            result = outilsSql.getResultSql(connection, "select nextval('seq_voyage')");
            this.setId("v" + result[0][0]);
            System.out.println("3");
            outilsSql.insertRow(this, "voyage");
            return new HttpResponse(204, "Insert voyage success.", null);
        }catch(Exception e) {
            System.out.println(e);
            return new HttpResponse(500, "Echec de l'insertion du voyage.", null);
        }finally{
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
