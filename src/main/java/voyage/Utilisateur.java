package voyage;

import dao.Colonne;
import dao.Table;

@Table(name="utilisateur")
public class Utilisateur {

    @Colonne(colonne="id")
    private String id;

    @Colonne(colonne="nom")
    private String nom;

    @Colonne(colonne="mdp")
    private String mdp;

    @Colonne(colonne="tel")
    private String tel;

    @Colonne(colonne="type")
    private String type;

    public Utilisateur(String id, String nom, String mdp, String tel, String type) {
        this.id = id;
        this.nom = nom;
        this.mdp = mdp;
        this.tel = tel;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
