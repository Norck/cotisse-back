package voyage;

import app.HttpResponse;
import dao.Connect;
import dao.OutilsSql;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class VoyageConfirme {
    private String id;
    private LocalDateTime dateConfirmation;

    public void setId(String id) {
        this.id = id;
    }

    public void setDateArrive(LocalDateTime dateArrive) {
        this.dateConfirmation = dateArrive;
    }

    public String getId() {
        return id;
    }

    public LocalDateTime getDateConfirmation() {
        return dateConfirmation;
    }

    public VoyageConfirme(String id, LocalDateTime dateConfirmation) {
        this.id = id;
        this.dateConfirmation = dateConfirmation;
    }

    public HttpResponse validerVoyage(){
        Connection connection = null;
        OutilsSql outilsSql = new OutilsSql();
        try{
            connection = Connect.connect("src/main/java/dao/postgresql.xml");
            if(connection == null){
                return new HttpResponse(503, "Echec de la connection à la base.", null);
            }
            connection.setAutoCommit(false);
            outilsSql.execute_query(connection, "update voyage set etat=11 where id='" + this.getId() + "'");
            outilsSql.insertRow(connection, this, "voyageConfirme");
            outilsSql.execute_query(connection, "commit");
            return new HttpResponse(204, "confirmation voyage réussite.", null);
        }catch(Exception e){
            return new HttpResponse(204, "Confirmation voyage échouée.", null);
        }finally{
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
