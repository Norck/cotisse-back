package voyage;

import app.HttpResponse;
import dao.Connect;
import dao.OutilsSql;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class ArriveVoyage {
    private String id;
    private LocalDateTime dateArrive;

    public void setId(String id) {
        this.id = id;
    }

    public void setDateArrive(LocalDateTime dateArrive) {
        this.dateArrive = dateArrive;
    }

    public String getId() {
        return id;
    }

    public LocalDateTime getDateArrive() {
        return dateArrive;
    }

    public ArriveVoyage(String id, LocalDateTime dateArrive) {
        this.id = id;
        this.dateArrive = dateArrive;
    }

    public HttpResponse setHeureArrive(){
        Connection connection = null;
        OutilsSql outilsSql = new OutilsSql();
        try{
            connection = Connect.connect("src/main/java/dao/postgresql.xml");
            if(connection == null){
                return new HttpResponse(503, "Echec de la connection à la base.", null);
            }
            String[][] result = outilsSql.getResultSql(connection, "select * from voyageValide where id='" + this.getId() + "'");
            if(result.length == 0){
                return new HttpResponse(204, "Voyage non existant ou annulé ou déjà terminé.", null);
            }
            outilsSql.insertRow(this, "arriveVoyage");
            return new HttpResponse(204, "Insertion heure arrivée réussite.", null);
        }catch(Exception e){
            return new HttpResponse(204, "Insertion heure arrivée échouée.", null);
        }finally{
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
