/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import data.Annotation.*;

/**
 *
 * @author hp
 */
public class Condition {
    @Attribut(colonne="etat")
    int etat;

    public Condition() {
    }

    public Condition(int etat) {
        this.etat = etat;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat)throws Exception {
        if(this.etat+this.getPas()<etat){
            throw new Exception("les etapes doivent etre respecté");
        }
        this.etat = etat;
    }
    
    public int getPas(){
        return 10;
    }
    
}
