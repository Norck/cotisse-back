/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import data.Annotation.*;
import data.Utilitaire.GeneriqueDAO;

/**
 *
 * @author hp
 */

@Classe(table="reservation")
public class Paiement {

        @PrimaryKey
        @Attribut(colonne="id")
        String id;
        
        @Attribut(colonne="idreservation")
        String idreservation;
        
        @Attribut(colonne="montant")
        Double montant;

    public Paiement() {
    }

    public Paiement(String id, String idreservation, Double montant) {
        this.id = id;
        this.idreservation = idreservation;
        this.montant = montant;
    }
    
    public Paiement( String idreservation, Double montant)throws Exception {
        this.id = GeneriqueDAO.getId("seq_paiement");
        this.idreservation = idreservation;
        this.montant = montant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdreservation() {
        return idreservation;
    }

    public void setIdreservation(String idreservation) {
        this.idreservation = idreservation;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }
        
}
