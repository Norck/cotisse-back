/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import data.Annotation.*;
import data.Utilitaire.GeneriqueDAO;

/**
 *
 * @author hp
 */
@Classe(table="reservationDetail")
public class ReservationDetail {
        @PrimaryKey
        @Attribut(colonne="id")
	String id;
        
        @Attribut(colonne="idreservation")
	String idreservation;
        
        @Attribut(colonne="idPlace")
	String idPlace;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdreservation() {
        return idreservation;
    }

    public void setIdreservation(String idreservation) {
        this.idreservation = idreservation;
    }

    public String getIdPlace() {
        return idPlace;
    }

    public void setIdPlace(String idPlace) {
        this.idPlace = idPlace;
    }

    public ReservationDetail(String idreservation, String idPlace)throws Exception {
        try{
            this.id = GeneriqueDAO.getId("seq_reservationDetail");
            this.idreservation = idreservation;
            this.idPlace = idPlace;
        }catch(Exception ex){
            throw ex;
        }
    }
        
}
