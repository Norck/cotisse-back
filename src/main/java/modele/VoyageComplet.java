/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import data.Annotation.*;
import java.time.LocalDateTime;

/**
 *
 * @author hp
 */
@Classe(table="voyageComplet")
public class VoyageComplet extends Voyage {
    @Attribut(colonne="departdesc")
    String descdepart;
    
    @Attribut(colonne="arrivedesc")
    String descarrive;
    
    @Attribut(colonne="marque")
    String marquevehicule;
    
    @Attribut(colonne="nbplace")
    int nbplace;
    
    @Attribut(colonne="descript")
    String type;
    
    @Attribut(colonne="multiplicateur")
    String multiplicateur;
    
    @Attribut(colonne="montant")
    Double montant;
    
    @Attribut(colonne="distance")
    Double distance;

    public VoyageComplet(String descdepart, String descarrive, String marquevehicule, int nbplace, String type, String multiplicateur, Double montant, Double distance, String id, String depart, String arrive, LocalDateTime dateVoyage, String idVehicule, int delaiPaiement) throws Exception {
        super(id, depart, arrive, dateVoyage, idVehicule, delaiPaiement);
        this.descdepart = descdepart;
        this.descarrive = descarrive;
        this.marquevehicule = marquevehicule;
        this.nbplace = nbplace;
        this.type = type;
        this.multiplicateur = multiplicateur;
        this.montant = montant;
        this.distance = distance;
    }


    public String getMultiplicateur() {
        return multiplicateur;
    }

    public void setMultiplicateur(String multiplicateur) {
        this.multiplicateur = multiplicateur;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
    
    public VoyageComplet(){
        super();
    }

    public String getDescdepart() {
        return descdepart;
    }

    public void setDescdepart(String descdepart) {
        this.descdepart = descdepart;
    }

    public String getDescarrive() {
        return descarrive;
    }

    public void setDescarrive(String descarrive) {
        this.descarrive = descarrive;
    }

    public String getMarquevehicule() {
        return marquevehicule;
    }

    public void setMarquevehicule(String marquevehicule) {
        this.marquevehicule = marquevehicule;
    }

    public int getNbplace() {
        return nbplace;
    }

    public void setNbplace(int nbplace) {
        this.nbplace = nbplace;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
