/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import data.Annotation.*;
import data.Utilitaire.GeneriqueDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author hp
 */
@Classe(table="reservation")
public class Reservation extends Condition {
        @PrimaryKey
        @Attribut(colonne="id")
	String id;
        
        @Attribut(colonne="idclient")
	String idclient;
        
        @Attribut(colonne="idvoyage")
	String idvoyage;
        
        @Attribut(colonne="datereservation")
	LocalDateTime datereservation;
        
        @Attribut(colonne="nbplace")
	int nbplace;

    public Reservation(String id, String idclient, String idvoyage, LocalDateTime datereservation, int nbplace, int etat) {
        super(etat);
        this.id = id;
        this.idclient = idclient;
        this.idvoyage = idvoyage;
        this.datereservation = datereservation;
        this.nbplace = nbplace;
    }

    public Reservation(String idclient, String idvoyage, LocalDateTime datereservation, int nbplace)throws Exception {
        super(1);
        try{
            this.id = GeneriqueDAO.getId("seq_reservation");
            this.idclient = idclient;
            this.idvoyage = idvoyage;
            this.datereservation = datereservation;
            this.nbplace = nbplace;
        }catch(Exception ex){
            throw ex;
        }
    }    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdclient() {
        return idclient;
    }

    public void setIdclient(String idclient) {
        this.idclient = idclient;
    }

    public String getIdvoyage() {
        return idvoyage;
    }

    public void setIdvoyage(String idvoyage) {
        this.idvoyage = idvoyage;
    }

    public LocalDateTime getDatereservation() {
        return datereservation;
    }

    public void setDatereservation(LocalDateTime datereservation) {
        this.datereservation = datereservation;
    }

    public int getNbplace() {
        return nbplace;
    }

    public void setNbplace(int nbplace) {
        this.nbplace = nbplace;
    }
    
    public static void reserver(Voyage voyage,String idUtilisateur,DetailPlace[] places,LocalDateTime dateReservation)throws Exception{
        Connection con=null;
        try{
            con=GeneriqueDAO.getConnection("src/main/java/data/Utilitaire/databaseConfig.xml");
            Reservation res=new Reservation(idUtilisateur,voyage.getId(),dateReservation,places.length);
            GeneriqueDAO.insert(res,false, con);
            for(DetailPlace dp : places){
                ReservationDetail resd=new ReservationDetail(res.getId(),dp.getPlace().getId());
                GeneriqueDAO.insert(resd,false, con);
                for(Option opt : dp.getOptions()){
                    opt.insertOptionDetail(resd.getId(), con);
                }
            }
            con.commit();
        }catch(Exception ex){
            if(con!=null){
                con.rollback();
            }
            throw ex;
        }finally{
            if(con!=null){
                con.close();
            }
        }
    }
    
    public void validerReservation()throws Exception{
        Connection con=null;
        try{
            con=GeneriqueDAO.getConnection("src/main/java/data/Utilitaire/databaseConfig.xml");
            this.setEtat(11);
            GeneriqueDAO.update(this, con);
            con.commit();
        }catch(Exception ex){
            if(con!=null){
                con.rollback();
            }
            throw ex;
        }finally{
            if(con!=null){
                con.close();
            }
        }
    }
    
    public void payerReservation(Double montant)throws Exception{
        Connection con=null;
        try{
            con=GeneriqueDAO.getConnection("src/main/java/data/Utilitaire/databaseConfig.xml");
            Double reste=this.getResteAPayer(con);
            if(montant>reste){
                throw new Exception("montant trop eleve");
            }
            if(Objects.equals(montant, reste)){
                this.setEtat(11);
                GeneriqueDAO.update(this, con);
            }
            Paiement p=new Paiement(this.getId(),montant);
            GeneriqueDAO.insert(p,false, con);
            con.commit();
        }catch(Exception ex){
            if(con!=null){
                con.rollback();
            }
            throw ex;
        }finally{
            if(con!=null){
                con.close();
            }
        }
    }
    
    public Double getTotalAPayer(Connection con)throws Exception{
        PreparedStatement prs=null;
        ResultSet rs=null;
        Double rep=0.0;
        try{
            prs=con.prepareStatement("SELECT (sum(montanttotal)+sum(montantoptions)) as total from prixReservationDetail where idReservation=? group by idReservation;");
            prs.setString(1,this.getId());
            rs=prs.executeQuery();
            while(rs.next()){
                rep=rs.getDouble(0);
                if(rs.wasNull()){
                    rep=0.0;
                }
            }
        }catch(Exception ex){
            throw ex;
        }
        return rep;
    }
    
    public Double getResteAPayer(Connection con)throws Exception{
        PreparedStatement prs=null;
        ResultSet rs=null;
        Double rep=0.0;
        try{
            prs=con.prepareStatement("SELECT sum(montant) as total from paiement where idReservation=?");
            prs.setString(1,this.getId());
            rs=prs.executeQuery();
            while(rs.next()){
                rep=rs.getDouble(0);
                if(rs.wasNull()){
                    rep=0.0;
                }
            }
            rep=this.getTotalAPayer(con)-rep;
        }catch(Exception ex){
            throw ex;
        }
        return rep;
    }
}
