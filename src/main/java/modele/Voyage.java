package modele;

import java.sql.Connection;

import app.HttpResponse;
import dao.Connect;
import dao.OutilsSql;
import data.Utilitaire.*;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
public class Voyage {

    public void setId(String id) {
        this.id = id;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public void setArrive(String arrive) {
        this.arrive = arrive;
    }

    public void setDateVoyage(LocalDateTime dateVoyage) {
        this.dateVoyage = dateVoyage;
    }

    public void setIdVehicule(String idVehicule) {
        this.idVehicule = idVehicule;
    }

    public void setDelaiPaiement(int delaiPaiement) {
        this.delaiPaiement = delaiPaiement;
    }

    public String getId() {
        return id;
    }

    public String getDepart() {
        return depart;
    }

    public String getArrive() {
        return arrive;
    }

    public LocalDateTime getDateVoyage() {
        return dateVoyage;
    }

    public String getIdVehicule() {
        return idVehicule;
    }

    public int getDelaiPaiement() {
        return delaiPaiement;
    }

    private String id;
    private String depart;
    private String arrive;
    private LocalDateTime dateVoyage;
    private String idVehicule;
    private int delaiPaiement;
    
    public Voyage(){
        
    }


    public Voyage(String id, String depart, String arrive, LocalDateTime dateVoyage, String idVehicule, int delaiPaiement) throws Exception {
        if(depart.contentEquals(arrive)){
            throw new Exception("Depart identique à arrivé.");
        }
        this.id = id;
        this.depart = depart;
        this.arrive = arrive;
        this.dateVoyage = dateVoyage;
        this.idVehicule = idVehicule;
        this.delaiPaiement = delaiPaiement;
    }

    public HttpResponse insertVoyage(){
        Connection connection = null;
        try{
            connection = Connect.connect("src/main/java/dao/postgresql.xml");
            OutilsSql outilsSql = new OutilsSql();
            if(connection == null){
                return new HttpResponse(503, "Echec de la connection à la base.", null);
            }
            String[][] result = outilsSql.getResultSql(connection, "select * from voyageEnCours where idVehicule='" + this.getIdVehicule() + "'");
            if(result.length > 0){
                return new HttpResponse(500, "Il y a déjà un voyage en cours.", null);
            }

            result = outilsSql.getResultSql(connection,"select date, lieu from voyageTermine where idVehicule='" + this.getIdVehicule() + "'");
            if(!result[0][1].contentEquals(this.getDepart())){
                return new HttpResponse(500, "Lieu de départ different du lieu de disponibilité.", null);
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateDisponibilite = LocalDateTime.parse(result[0][0], formatter);
            if( dateDisponibilite.isAfter(this.getDateVoyage() )) {
                return new HttpResponse(500, "Voyage avant disponibilité voiture.", null);
            }

            result = outilsSql.getResultSql(connection, "select nextval('seq_voyage')");
            this.setId("v" + result[0][0]);
            outilsSql.insertRow(this, "voyage");
            return new HttpResponse(204, "Insert voyage success.", null);
        }catch(Exception e) {
            System.out.println(e);
            return new HttpResponse(500, "Echec de l'insertion du voyage.", null);
        }finally{
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static Voyage[] getListeVoyage(LocalDateTime date1,LocalDateTime date2)throws Exception{
        Connection con=null;
        Voyage[] rep=null;
        try{
            con=GeneriqueDAO.getConnection("src/main/java/data/Utilitaire/databaseConfig.xml");
            Object[] obj=GeneriqueDAO.selectWithObject(new VoyageComplet(),"dateVoyage>='"+date1.toString()+"' and dateVoyage<='"+date2.toString()+"'",con);
            rep=new Voyage[obj.length];
            for(int i=0;i<obj.length;i++){
                rep[i]=(VoyageComplet)obj[i];
            }
        }catch(Exception ex){
            throw ex;
        }finally{
            if(con!=null){
                con.close();
            }
        }
        return rep;
    }
}
