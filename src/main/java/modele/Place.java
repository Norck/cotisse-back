/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import data.Annotation.*;

/**
 *
 * @author hp
 */
@Classe(table="place")
public class Place extends Condition{
        @Attribut(colonne="id")
	String id;
        
        @Attribut(colonne="idVehicule")
	String idVehicule;
        
        @Attribut(colonne="rang")
	String rang;
        

    public Place(String id, String idVehicule, String rang, int etat) {
        super(etat);
        this.id = id;
        this.idVehicule = idVehicule;
        this.rang = rang;
    }

    public Place() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVehicule() {
        return idVehicule;
    }

    public void setIdVehicule(String idVehicule) {
        this.idVehicule = idVehicule;
    }

    public String getRang() {
        return rang;
    }

    public void setRang(String rang) {
        this.rang = rang;
    }

}
