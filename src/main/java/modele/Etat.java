/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import data.Annotation.*;

/**
 *
 * @author hp
 */
@Classe(table="etat")
public class Etat {
        @PrimaryKey()
        @Attribut(colonne="id")
        String id;
        
        @Attribut(colonne="nombre")
        int nb;

    public Etat() {
    }

    public Etat(String id, int nb) {
        this.id = id;
        this.nb = nb;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNb() {
        return nb;
    }

    public void setNb(int nb) {
        this.nb = nb;
    }
        
}
