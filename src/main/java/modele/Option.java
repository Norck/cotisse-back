/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import data.Annotation.*;
import data.Utilitaire.GeneriqueDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author hp
 */
@Classe(table="options")
public class Option {
        @PrimaryKey()
        @Attribut(colonne="id")
        String id;
        
        @Attribut(colonne="descript")
	String descript;
        
        @Attribut(colonne="prix")
	Double prix;

    public Option() {
    }

    public Option(String id, String descript, Double prix) {
        this.id = id;
        this.descript = descript;
        this.prix = prix;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
    
    public void insertOptionDetail(String idReservationDetail,Connection con)throws Exception{
        PreparedStatement prs=null;
        try{
            con=GeneriqueDAO.getConnection("src/main/java/data/Utilitaire/databaseConfig.xml");
            prs=con.prepareStatement("insert into optionReservation values('"+this.id+"','"+idReservationDetail+"')");
            prs.executeUpdate();
        }
        catch(Exception ex){
            throw ex;
        }
        finally{
            if(prs!=null){
                prs.close();
            }
        }
    }
}
