package dao;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.File;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.sql.Connection;
import java.sql.DriverManager;

public class Connect {
	
	public Connect(String database, String port, String base) {
		
	}
	
	public static Connection connect(String fileName) {
		Connection conn = null;
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);

			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new File(fileName));
			
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("configuration");
			Node node = nList.item(0);
			Element elt = (Element) node;

			String sgbd = elt.getElementsByTagName("sgbd").item(0).getTextContent();
			String ip = elt.getElementsByTagName("ip").item(0).getTextContent();
			String port = elt.getElementsByTagName("port").item(0).getTextContent();
			String database = elt.getElementsByTagName("database").item(0).getTextContent();
			String user = elt.getElementsByTagName("user").item(0).getTextContent();
			String password = elt.getElementsByTagName("password").item(0).getTextContent();
			
			if(sgbd.contentEquals("postgresql")) {
				Class.forName("org.postgresql.Driver");
			}else if(sgbd.contentEquals("oracle")) {
				Class.forName("oracle.jdbc.driver.OracleDriver");
			}
			String url = "jdbc:" + sgbd + ":" + "//" + ip + ":" + port + "/" + database;
			conn = DriverManager.getConnection(url,user,password);
		}
		catch(Exception e) {
			System.out.println(e);
		}
		return conn;
	}
}
