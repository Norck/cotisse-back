package dao;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.lang.IllegalAccessException;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;
import java.time.LocalDateTime;
public class GenericDAO {
	public int nbMaxResult;
	public String dbConfig;

	public GenericDAO(int nbMaxResult, String dbConfig){
		this.nbMaxResult = nbMaxResult;
		this.dbConfig = dbConfig;
	}

	public String getSgbd(){
		String sgbd = null;
		try{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);

			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new File(dbConfig));

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("configuration");
			Node node = nList.item(0);
			Element elt = (Element) node;

			sgbd = elt.getElementsByTagName("sgbd").item(0).getTextContent();
		}catch(Exception e){
			throw(e);
		}finally{
			return sgbd;
		}
	}

	private static String[] getColumnNames(ResultSet resultset) throws SQLException{
		int i = 0;
		ResultSetMetaData rsmd = resultset.getMetaData();
		int nbrColumn = rsmd.getColumnCount();
		String[] column_name = new String[nbrColumn];
		for(i = 0; i < nbrColumn; i++){
			column_name[i] = rsmd.getColumnName(i + 1);
		}

		return column_name;
	}

	private String[] FieldsName (Field[] fields){
		String[] names = new String[fields.length];
		for(int i = 0; i < fields.length; i++){
			names[i] = fields[i].getName();
		}
		return names;
	}

	private static String[] getNomColonnesTable(Connection connect, String nomTable)throws Exception{
		// RESSERVATION DE LA RESSOURCE
		Statement statement  =  connect.createStatement();
		try{
			//OBTENTION DES NOMS DES COLONNES
			int i = 0, u =0;
			String query = "select * from " + nomTable + " limit 1";
			ResultSet resultset = statement.executeQuery(query);
			String[] column_name = getColumnNames(resultset);

			return column_name;
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			statement.close();
		}
		String[] vide = new String[0];
		return vide;
	}

	private Object[] get_fieldsValue(Object object, Field[] tabField)throws NoSuchMethodException, IllegalAccessException, InvocationTargetException{
		Class cl = object.getClass();
		Method mtd = null;
		Object[] value = new Object[tabField.length];
		for(int i = 0; i < tabField.length; i++){
			mtd = cl.getMethod( "get" + Outils.toUperCase0(tabField[i].getName()) );
			value[i] = mtd.invoke(object);
		}
		return value;
	}

	private String[] get_fieldsType(Field[] fields){
		String[] type = new String[fields.length];
		for(int i = 0; i < fields.length; i++){
			type[i] = fields[i].getType().getName();
		}

		return type;
	}

	private Vector<Integer> get_indices_fields_concernes(String[] column_name, String[] fieldsName){
		int i = 0, u = 0;
		boolean equality = false;
		Vector<Integer> indices_concernes = new Vector<Integer>(5, 5);
		//boucle pour parcourir les noms de colonnes
		for(i = 0; i < column_name.length; i++){

			//boucle pour parcourir les noms de fields
			for(u = 0; u < fieldsName.length; u++){
				equality = column_name[i].equalsIgnoreCase(fieldsName[u]);
				if(equality == true){
					indices_concernes.add(u);
					break;
				}
			}
		}

		return indices_concernes;
	}

	private String construct_request_find(String nomTable, String[] fieldsName, String[] fieldsType, Object[] fieldsValue){
		// La requete finale sera de la forme
		// SELECT * FROM nomTable WHERE a = 'a1' and b = 'b1' and c = 'c1' etc
		int i = 0;
		boolean valueNull = true;
		if(fieldsValue != null){
			for(int u = 0; u < fieldsValue.length; u++){
				if(fieldsValue[u] != null){
					valueNull = false;
					break;
				}
			}
		}
		String contour = "'";
		String suite = " and ";
		String requete = "select * from " + nomTable;
		if(valueNull == false){
			requete += " where ";
			for(i = 0; i < fieldsName.length; i++){
				if(i == fieldsName.length - 1){
					suite = "";
				}
				contour = "'";
				if(fieldsType[i] == "Integer"){
					contour = "";
				}
				if(fieldsType[i] == "LocalDateTime"){

				}
				String value_string = String.valueOf(fieldsValue[i]);
				requete += fieldsName[i] + "=" + contour + value_string + contour + suite;
			}
		}
		return requete;
	}

	private String construct_request_insert(Connection connection, String nomTable, String[] fieldsName, Object[] fieldsValue, String[] pkInfo)throws Exception{
		Outils outils = new Outils();

		String requete = "insert into " + nomTable + " (";
		String suite = ", ";
		String contour1 = "'";
		String contour2 = "'";
		for(int i = 0; i < fieldsName.length; i++){
			if(i == fieldsName.length - 1){
				suite = ") values (";
			}
			requete += fieldsName[i] + suite;
		}
		suite = ",";
		for(int i = 0; i < fieldsName.length; i++){
			if(i == fieldsName.length - 1){
				suite = ")";
			}
			String value = String.valueOf(fieldsValue[i]);
			if(pkInfo.length != 0){
				if (fieldsName[i].contentEquals(pkInfo[0])) {
					String[][] pk = getResultSql(connection, "select nextval('" + pkInfo[2] + "')");
					value = pkInfo[1] + pk[0][0];
				}
			}
			if(fieldsValue[i] instanceof Integer || fieldsValue[i] instanceof Float || fieldsValue[i] instanceof Double){
				contour1 = "";
				contour2 = "";
			}
			else if(fieldsValue[i] instanceof LocalDateTime){
				try{
					value = outils.dateToStringDate(value);
				}
				catch(Exception e){
					throw new Exception("insertRow : " + e);
				}
			}
			requete += "'" + value + "'" + suite;
			contour1 = contour2 = "'";
		}

		return requete;
	}

	private void execute_query(Connection connect, String query)throws Exception{
		Statement statement = connect.createStatement();
		System.out.println(query);
		try{
			PreparedStatement preparedStatement  = connect.prepareStatement(query);
			preparedStatement.executeUpdate();
		}
		catch(Exception e){
			if(!e.getMessage().contentEquals("Aucun résultat retourné par la requête.")
			|| !e.getMessage().contentEquals("Aucun resultat ou requete incorrecte !")) {
				throw(e);
			}
		}
		finally{
			statement.close();
		}
	}

	private void execute_query_transactionel(Connection connect, String query)throws Exception{
		Statement statement = connect.createStatement();
		System.out.println(query);
		try{
			statement.executeQuery(query);
		}
		catch(Exception e){
			if(!e.getMessage().contentEquals("Aucun résultat retourné par la requête.")) {
				throw(e);
			}
		}
		finally{
			statement.close();
		}
	}

	private static Object[] tabString_to_tabObject(String[] tab){
		Object[] o = new Object[tab.length];
		for(int i = 0; i < tab.length; i++){
			o[i] = tab[i];
		}

		return o;
	}

	private String[][] tabVectorToString(Vector<Object[]> tab){

		Object[] temp_obj = (Object[]) tab.elementAt(0);
		int nbr_row = tab.size();
		int nbr_column = temp_obj.length;

		String[][]  tab_string= new String[nbr_row][nbr_column];

		//premiere boucle pour parcourir les lignes
		for(int i = 0; i < nbr_row; i++){
			Object[] temp_obj1 = (Object[]) tab.elementAt(i);
			//dexieme boucle pour parcourir les colonnes
			for(int u = 0; u < nbr_column; u++){
				tab_string[i][u] = temp_obj1[u].toString();
			}
		}

		return tab_string;
	}

	private Object[] resultToObj(Object object, String[] column_name, String[][] result) throws Exception {
		Object[] tabObj = new Object[result.length];
		Object[][] column_field = getColonnes( object.getClass().getDeclaredFields() );
		String[] colonnes = new String[column_field.length];
		Field[] fields = new Field[column_field.length];
		for(int i = 0; i < fields.length; i++){
			colonnes[i] = (String) column_field[i][0];
			fields[i] = (Field) column_field[i][1];
		}
		String[] fieldsType = get_fieldsType(fields);
		int[] indexColumn = new int[colonnes.length];
		for(int i = 0; i < colonnes.length; i++){
			for(int u = 0; u < column_name.length; u++){
				if(colonnes[i].contentEquals(column_name[u])){
					indexColumn[i] = u;
				}
			}
		}
		Class cl = object.getClass();
		for(int i = 0; i < result.length; i++){
			tabObj[i] =object.getClass().newInstance();
			for(int u = 0; u < column_name.length; u++){
				for(int v = 0; v < fields.length; v++){
					if(fields[v].getName().contentEquals(column_name[u])){
						if(fieldsType[v].contentEquals("java.lang.String")){
							Method mtdSet = cl.getMethod("set" + Outils.toUperCase0(fields[v].getName()), String.class);
							mtdSet.invoke(tabObj[i], result[i][indexColumn[v]]);
						}else if(fieldsType[v].contentEquals("int")){
							Method mtdSet = cl.getMethod("set" + Outils.toUperCase0(fields[v].getName()), fields[v].getType());
							mtdSet.invoke(tabObj[i], Integer.parseInt( result[i][indexColumn[v]] ));
						}else if(fieldsType[v].contentEquals( "float")){
							Method mtdSet = cl.getMethod("set" + Outils.toUperCase0(fields[v].getName()), Float.class);
							mtdSet.invoke(tabObj[i], Float.parseFloat( result[i][indexColumn[v]]) );
						}else if(fieldsType[v].contentEquals("java.sql.Date")){
							Method mtdSet = cl.getMethod("set" + Outils.toUperCase0(fields[v].getName()), Date.class);
							mtdSet.invoke(tabObj[i], Date.valueOf( result[i][indexColumn[v]] ));
						}
						break;
					}
				}
			}
		}
		return tabObj;
	}

	public Object[] find(Connection connection, Class cl, String where,int nbContent, int nbPage) throws Exception {
		String nomTable = ( (Table) cl.getAnnotation(Table.class)).name();
		Object[][] column_field = getColonnes(cl.getDeclaredFields());
		String[] columnTable = getNomColonnesTable(connection, nomTable);
		String[] columns = new String[column_field.length];
		Field[] fields = new Field[column_field.length];
		Object[] fieldsValue = null;
		String[] fieldsType = new String[column_field.length];
		for(int i = 0; i < columns.length; i++){
			columns[i] = (String) column_field[i][0];
			fields[i] = (Field) column_field[i][1];
			fieldsType[i] = fields[i].getType().getName();
			Method mtd = cl.getMethod("get" + Outils.toUperCase0(fields[i].getName()));
		}
		String query = construct_request_find(nomTable, columns, fieldsType, fieldsValue);
		if(where != null){
			query += " where " + where;
		}
		String sgbd = getSgbd();
		if(sgbd.contentEquals("postgresql") || sgbd.contentEquals("mysql")){
			if(nbContent != 0){
				query += " offset " + nbContent * (nbPage - 1) + " limit " + nbContent;
			}
		}
		System.out.println(query);
		String[][] result = getResultSql(connection, query);
		Object[] tabObj = resultToObj(cl.newInstance(), columnTable, result);

		return tabObj;
	}

	//find ne ferme pas la connection qu'elle utilise
	//objet contient tous les donnees qu'il faut rechercher
	public Object[] find(Connection connect, Object object, String[] fieldTargeted, int nbPage)throws Exception{
		Class cl = object.getClass();
		String nomTable = object.getClass().getAnnotation(Table.class).name();
		String[] column_name = getNomColonnesTable(connect,nomTable);
		Field[] tabFields = object.getClass().getDeclaredFields();
		String[] fieldsType = new String[0];
		Object[] fieldsValue = new Object[0];

		String[] annoColonne = new String[0];
		if(fieldTargeted != null) {
			Field[] tempField = new Field[fieldTargeted.length];
			for(int i = 0; i < tempField.length; i++) {
				tempField[i] = object.getClass().getDeclaredField(fieldTargeted[i]);
			}
			Object[][] column_field = getColonnes(tempField);
			String[] columns = new String[column_field.length];
			Field[] fields = new Field[column_field.length];
			for(int i = 0; i < column_field.length; i++){
				columns[i] = (String) column_field[i][0];
				fields[i] = (Field) column_field[i][1];
			}
			annoColonne = columns;
			tabFields = fields;
		}else {
			Object[][] column_field = getColonnes(tabFields);//ovaina [0]=colonnes, [1]=field nahazoana azy
			String[] columns = new String[column_field.length];
			Field[] fields = new Field[column_field.length];
			for(int i = 0; i < fields.length; i++){
				columns[i] = (String) column_field[i][0];
				fields[i] = (Field) column_field[i][1];
			}
			annoColonne = columns;
			tabFields = fields;
		}
		fieldsType = get_fieldsType(tabFields);
		fieldsValue = get_fieldsValue(object, tabFields);

		//CONSTRUCTION DE LA REQUETE FIND
		String query = construct_request_find(nomTable, annoColonne, fieldsType, fieldsValue);
		//String query = "select * from personne";
		String sgbd = getSgbd();
		if(sgbd.contentEquals("postgresql") || sgbd.contentEquals("mysql")){
			query += " offset " + nbMaxResult * (nbPage - 1) + " limit " + nbMaxResult;
		}else if(sgbd.contentEquals("oracle"))

		//EXECUTION DE LA REQUETE FIND
		System.out.println(query);
		String[][] result = getResultSql(connect, query);

		Object[] tabObj = new Object[0];
		try{
			tabObj = resultToObj(object, column_name, result);
		}catch(Exception e){
			throw(e);
		}
		return tabObj;
	}

	public void update(Connection connection, Object object) throws Exception {
		Field[] dfields = object.getClass().getDeclaredFields();
		Object[][] column_field = getColonnes(dfields);
		String[] columns = new String[column_field.length];
		Field[] fields = new Field[column_field.length];
		for(int i = 0; i < column_field.length; i++){
			columns[i] = (String) column_field[i][0];
			fields[i] = (Field) column_field[i][1];
		}
		String column_pk = null;
		Object pk_value = null;
		Class cl = object.getClass();
		boolean pk_found = false;
		boolean ispk = false;
		String query = "update " + object.getClass().getAnnotation(Table.class).name() + " set ";
		for(int i = 0; i < fields.length; i++){
			ispk = false;
			if(!pk_found){
				try {
					PrimaryKey pk = (PrimaryKey) fields[i].getAnnotation(PrimaryKey.class);
					Field[] tempTab = new Field[1];
					tempTab[0] = fields[i];
					column_field = getColonnes(tempTab);
					column_pk = (String) column_field[0][0];
					pk_value = cl.getMethod("get" + Outils.toUperCase0(fields[i].getName())).invoke(object);
					pk_found = true;
					ispk = true;
				}catch(Exception e){

				}
			}
			if(!ispk){
				column_field = getColonnes(new Field[]{fields[i]});
				String column = (String) column_field[0][0];
				Object value = cl.getMethod("get" + Outils.toUperCase0(fields[i].getName())).invoke(object);
				String contour1 = "'";
				String contour2 = "'";
				if((value instanceof Integer) || (value instanceof Float) ||(value instanceof Double) ){
					contour2 = contour1 = "";
				}
				if(value instanceof LocalDateTime)
					try{
						value = new Outils().dateToStringDate(value.toString());
						contour1 = "to_date('";
						contour2 = "', 'YYYY-MM-DD HH24:MI:SS')";
						query += column + "=" + contour1 + value + contour2;
						continue;
					}
					catch(Exception e){
						throw new Exception("update : " + e);
					}
				query += column + "=" + contour1 + value + contour2;
				if(i != fields.length - 1){
					query += " , " ;
				}
			}

		}
		if((pk_value instanceof Integer) || (pk_value instanceof Float) || (pk_value instanceof Double)){
			query += " where " + column_pk + "=" + pk_value;
		}else{
			query += " where " + column_pk + "='" + pk_value + "'";
		}
		execute_query(connection, query);
	}


	private String[] getPkInfo(Field[] tabField){
		String[] result = new String[0];
		for(int i = 0; i < tabField.length; i++){
			try {
				PrimaryKey pk = tabField[i].getAnnotation(PrimaryKey.class);
				String[] pkInfo = new String[3];
				pkInfo[0] = tabField[i].getName();
				pkInfo[1] = pk.beforeSeq();
				pkInfo[2] = pk.sequenceName();
				result = pkInfo;
				break;
			}
			catch(Exception e){

			}
		}
		return result;
	}

	private Object[][] getColonnes(Field[] tabField) throws Exception {
		ArrayList<String> listColonnes = new ArrayList<String>();
		ArrayList<Field> listFields = new ArrayList<Field>();
		for(int i = 0; i < tabField.length; i++) {
			try {
				listColonnes.add( tabField[i].getAnnotation(Colonne.class).colonne() );
				listFields.add(tabField[i]);
			}catch(Exception e) {

			}
		}
		if(listColonnes.size() == 0){
			throw new Exception("Aucune colonne correspondant à un attribut trouvée.");
		}
		Object[][] result = new Object[listColonnes.size()][2];
		for(int i = 0; i < result.length; i++){
			result[i][0] = listColonnes.get(i);
			result[i][1] = listFields.get(i);
		}
		return result;
	}

	public void insert(Connection connect, Object object)throws Exception{
		String nomTable = object.getClass().getAnnotation(Table.class).name();
		String[] column_name = getNomColonnesTable(connect,nomTable);
		Field[] tabFields = object.getClass().getDeclaredFields();
		String[] fieldsName = FieldsName(tabFields);
		Object[] fieldsValue = new Object[0];

		Object[][] column_field = getColonnes(tabFields);//ovaina [0]=colonnes, [1]=field nahazoana azy
		String[] columns = new String[column_field.length];
		Field[] fields = new Field[column_field.length];
		for(int i = 0; i < fields.length; i++){
			columns[i] = (String) column_field[i][0];
			fields[i] = (Field) column_field[i][1];
		}

		fieldsValue = get_fieldsValue(object, tabFields);
		String[] pkInfo = getPkInfo(tabFields);
		String insert = construct_request_insert(connect, nomTable, columns, fieldsValue, pkInfo);
		System.out.println(insert);
		//EXECUTION DE LA REQUETE INSERT
		execute_query(connect, insert);
	}

	public static String[][] getResultSql(Connection connection ,String sql) throws Exception {
		Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
		try {
			ResultSet result = statement.executeQuery(sql);
			result.last();
			int nbR = result.getRow();
			ResultSetMetaData metadata = result.getMetaData();
			int nbC = metadata.getColumnCount();
			result.beforeFirst();
			String[][] tab = new String[nbR][nbC];
			int i = 0;
			while(result.next()) {
				for(int j=0;j<nbC;j++){
					tab[i][j] = result.getString(j+1);
				}
				i++;
			}

			return tab;
		} catch(SQLException e) {
			throw new Exception("Aucun resultat ou requete incorrecte !");
		} finally {
			statement.close();
		}
	}
}
