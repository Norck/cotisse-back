package dao;

import java.lang.Character;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.lang.IllegalAccessException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.Vector;
import java.time.LocalDateTime;
public class OutilsSql{

	public static String[] getColumnNames(ResultSet resultset) throws SQLException{
		int i = 0;
		ResultSetMetaData rsmd = resultset.getMetaData();
		int nbrColumn = rsmd.getColumnCount();
		String[] column_name = new String[nbrColumn];
		for(i = 0; i < nbrColumn; i++){
			column_name[i] = rsmd.getColumnName(i + 1);
		}

		return column_name;
	}

	// pour convertir un tableau de string en vector
	public Vector<String> StToVect(String[] tab){
		Vector<String> result = new Vector<String>(5, 5);
		for(int i = 0; i < tab.length; i++){
			String temp = tab[i];
			result.add(temp);
		}

		return result;
	}

	public String[] FieldsName (Field[] fields){
		System.out.println("fields = " + fields.length);
		String[] names = new String[fields.length];
		for(int i = 0; i < fields.length; i++){
			names[i] = fields[i].getName();
		}
		return names;
	}

	// RETOURNE LE NOM DU METHOD GET CORRESPONDANT AU NOM DU FIELD	
	public String NameMethodGet(String fieldName){
		String get = "get";
		String method  = get.concat(fieldName);

		return method;
	}

	public static String[] getNomColonnesTable(Connection connect, String nomTable)throws Exception{
		// RESSERVATION DE LA RESSOURCE
		Statement statement  =  connect.createStatement();
		try{
			//OBTENTION DES NOMS DES COLONNES
			int i = 0, u =0;
			String query = "select * from " + nomTable;
			ResultSet resultset = statement.executeQuery(query);
			String[] column_name = getColumnNames(resultset);

			return column_name;	
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			statement.close();
		}
		String[] vide = new String[0];
		return vide;
	}

	//retourne un tableau de field
	public Field[] get_fields(Object object){
		//OBTENTION DES FIELDS
		Class cl = object.getClass();
		Field[] tabFields = cl.getDeclaredFields();

		return tabFields;
	}

	// retourne le nom probable du methode get d'un field
	public String construct_nameMtdGet(String fieldsName){
		char[] name = fieldsName.toCharArray();
		if(Character.isLowerCase(name[0]) == true){
			name[0] = Character.toUpperCase(name[0]);
			fieldsName = new String(name);
			fieldsName = "get" + fieldsName;  
		}
		else{
			fieldsName = "get" + fieldsName;
		}
		return fieldsName;
	}

	public Vector<Method> get_mtdGet(Object object)throws NoSuchMethodException{
		Class cl = object.getClass();
		Vector<Method> mtdGet = new Vector(5, 5);
		Field[] fields = get_fields(object);
		String[] fieldsName = FieldsName(fields);
		for(int i = 0; i < fieldsName.length; i++){
			String nomGetMtd = construct_nameMtdGet(fieldsName[i]);
			Method temp_met = cl.getMethod(nomGetMtd);
			mtdGet.add(temp_met);
		}

		return mtdGet;
	}


	public Vector<Object> get_fieldsValue(Object object)throws NoSuchMethodException, IllegalAccessException, InvocationTargetException{
		Vector<Method> mtdGet = get_mtdGet(object);
		Vector<Object> fieldsValue = new Vector(5, 5);
		int size =  mtdGet.size();
		for(int i = 0; i < size; i++){
			Method mtd = (Method) mtdGet.elementAt(i);
			Object value = mtd.invoke(object);
			fieldsValue.add(value);
		}

		return fieldsValue;
	}

	public String[] get_fieldsType(Field[] fields){
		String[] type = new String[fields.length];
		for(int i = 0; i < fields.length; i++){
			type[i] = fields[i].getType().getName();
		}

		return type;
	}

	public Vector<Integer> get_indices_fields_concernes(String[] column_name, String[] fieldsName){
		int i = 0, u = 0;
		boolean equality = false;
		Vector<Integer> indices_concernes = new Vector<Integer>(5, 5);
		//boucle pour parcourir les noms de colonnes
		for(i = 0; i < column_name.length; i++){

			//boucle pour parcourir les noms de fields
			for(u = 0; u < fieldsName.length; u++){
				equality = column_name[i].equalsIgnoreCase(fieldsName[u]);
				if(equality == true){
					indices_concernes.add(u);
					break;
				}
			}
		}

		return indices_concernes;
	}

	public String construct_request_find(String nomTable, String[] fieldsName, String[] fieldsType, Vector<Object> fieldsValue, Vector<Integer> indices_utilises){
		// La requete finale sera de la forme
		// SELECT * FROM nomTable WHERE a = 'a1' and b = 'b1' and c = 'c1' etc
		int i = 0, size = indices_utilises.size();
		String contour = "'";
		String suite = " and ";
		String requete = "select * from " + nomTable + " where ";
		
		for(i = 0; i < size; i++){
			int indice = (int) indices_utilises.elementAt(i);
			Object value = (Object) fieldsValue.elementAt(indice);
			if(i == size - 1){
				suite = "";
			}
			contour = "'";
			if(fieldsType[indice] == "Integer"){
				contour = "";
			}
			Object o = (Object) fieldsValue.elementAt(indice);
			String value_string = String.valueOf(o);
			requete += fieldsName[indice] + "=" + contour + value_string + contour + suite; 
		}

		return requete;
	}

	public String construct_request_insert(String nomTable, String[] fieldsName, Vector<Object> fieldsValue, Vector<Integer> indices_utilises)throws Exception{
		OutilsSql outilsSql = new OutilsSql();
		Outils outils = new Outils();

		String requete = "insert into " + nomTable + " (";
		String suite = ", ";
		String contour1 = "'";
		String contour2 = "'";
		int size = indices_utilises.size();
		for(int i = 0; i<size; i++){
			int indice = (int) indices_utilises.elementAt(i);
			if(i == size - 1){
				suite = ") values (";
			}
			requete += fieldsName[indice] + suite;
		}
		suite = ",";
		for(int i = 0; i<size; i++){
			int indice = (int) indices_utilises.elementAt(i);
			if(i == size - 1){
				suite = ")";
			}
			Object o = (Object) fieldsValue.elementAt(indice);
			String value = String.valueOf(o);
			if(fieldsValue.elementAt(indice) instanceof Integer ||
				fieldsValue.elementAt(indice) instanceof Float ||
				fieldsValue.elementAt(indice) instanceof Double){
				contour1 = "";
				contour2 = "";
			}
			else if(fieldsValue.elementAt(indice) instanceof LocalDateTime){
				try{
					value = outils.dateToStringDate(value);
					System.out.println(value);
				}
				catch(Exception e){
					throw new Exception("insertRow : " + e);
				}
				contour1 = "to_date('";
				contour2 = "', 'YYYY-MM-DD HH24:MI:SS')";
			}
			requete += contour1 + value + contour2 + suite;
			contour1 = contour2 = "'";
		}

		return requete;
	}


	public void execute_query(Connection connect, String query)throws Exception{
		Statement statement = connect.createStatement();
		System.out.println(query);
		try{
			PreparedStatement preparedStatement  = connect.prepareStatement(query);
			preparedStatement.executeUpdate();
		}
		catch(Exception e){
			if(!e.getMessage().contentEquals("Aucun résultat retourné par la requête.")) {
				throw(e);
			}
		}
		finally{
			statement.close();
		}
	}
	
	public void execute_query_transactionel(Connection connect, String query)throws Exception{
		Statement statement = connect.createStatement();
		System.out.println(query);
		try{
			statement.executeQuery(query);
		}
		catch(Exception e){
			if(!e.getMessage().contentEquals("Aucun résultat retourné par la requête.")) {
				throw(e);
			}
		}
		finally{
			statement.close();
		}
	}

	public static Object[] tabString_to_tabObject(String[] tab){
		Object[] o = new Object[tab.length];
		for(int i = 0; i < tab.length; i++){
			o[i] = tab[i];
		}

		return o;
	}

	public String[][] tabVectorToString(Vector<Object[]> tab){

		Object[] temp_obj = (Object[]) tab.elementAt(0);
		int nbr_row = tab.size();
		int nbr_column = temp_obj.length;

		String[][]  tab_string= new String[nbr_row][nbr_column];

		//premiere boucle pour parcourir les lignes
		for(int i = 0; i < nbr_row; i++){
			Object[] temp_obj1 = (Object[]) tab.elementAt(i);
			//dexieme boucle pour parcourir les colonnes
			for(int u = 0; u < nbr_column; u++){
				tab_string[i][u] = temp_obj1[u].toString();
			}
		}

		return tab_string;
	}

	public Vector<Object[]> execute_select_query(Connection connect, String query)throws SQLException, NoSuchMethodException{
		
		// RESSERVATION DE LA RESSOURCE
		Statement statement  =  connect.createStatement();
		Vector<Object[]> resultat = new Vector<Object[]>(5,5);
		System.out.println(query);
		try{
			//OBTENTION DES NOMS DES COLONNES
			int i = 0, u = 0;
			ResultSet resultset = statement.executeQuery(query);
			System.out.println(query);
			ResultSetMetaData rsmd = resultset.getMetaData();
	     	int nbrColumn = rsmd.getColumnCount();
			while(resultset.next() == true){
				Object[] temp_object = new Object[nbrColumn];

				for(i = 0; i < nbrColumn; i++){
					temp_object[i] = resultset.getObject(i+1);
				}
				resultat.add(temp_object);
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		finally{
			statement.close();
			return resultat;
		}
	}
	
	public Vector<Integer> removeNullValue(Vector<Object> fieldsValue, Vector<Integer> indices_fields_concernes){
		Vector<Integer> result = new Vector<Integer>(5, 5);
		for(int i = 0; i < indices_fields_concernes.size(); i++) {
			int index = (int) indices_fields_concernes.get(i);
			Object fieldValue = fieldsValue.get(index);
			if(fieldValue != null) {
				result.add(index);
			}
		}
		
		return result;
	}

	//find ne ferme pas la connection qu'elle utilise
	//objet contient tous les donnees qu'il faut rechercher
	public String[][] find(Connection connect, Object object, String nomTable)throws Exception{
		//OBTENTION DES NOMS DE COLONNES DU TABLE
		String[] column_name = getNomColonnesTable(connect,nomTable);

		//OBTENTION DES NOMS DE FIELDS, TYPES DE FIELDS ET DES VALEURS DES FIELDS
		Field[] tabFields = get_fields(object);
		String[] fieldsName = FieldsName(tabFields);
		String[] fieldsType = get_fieldsType(tabFields);
		Vector<Object> fieldsValue = get_fieldsValue(object);
		
		String[] annoColonne = getColonnes(tabFields);

		//OBTENTION DES INDICES DE FIELDS A UTILISER
		Vector<Integer> indices_fields_concernes =  get_indices_fields_concernes(column_name, annoColonne);
		
		//ENLEVEMENT DES FIELDS AVEC VALEURS NULS
		indices_fields_concernes = removeNullValue(fieldsValue, indices_fields_concernes);
		
		//CONSTRUCTION DE LA REQUETE FIND
		String query = construct_request_find(nomTable, annoColonne, fieldsType, fieldsValue, indices_fields_concernes);

		//EXECUTION DE LA REQUETE FIND
		System.out.println(query);
		String[][] result = getResultSql(connect, query);
		
		return result;
	}

	public String[] getColonnes(Field[] tabField) {
		String[] annoColonne = new String[tabField.length];
		for(int i = 0; i < tabField.length; i++) {
			try {
				annoColonne[i] = tabField[i].getAnnotation(Colonne.class).colonne();
			}catch(Exception e) {
				annoColonne[i] = tabField[i].getName();
			}
		}
		return annoColonne;
	}

	public void insertRow(Object object, String nomTable)throws Exception{
		Connection connection = null;
		try{
			connection = Connect.connect("src/main/java/dao/postgresql.xml");
			insertRow(connection, object, nomTable);
		}catch(Exception e){
			throw(e);
		}finally{
			if(connection != null){
				connection.close();
			}
		}

	}

	//on peut avoir le nom des colonne meme si on n'a pas encore insere des donnees
	//insert un objet dans un tableau du base
	//insertRow ne ferme pas la connection qu'elle utilise
	public void insertRow(Connection connect, Object object, String nomTable)throws Exception{
		//OBTENTION DES NOMS DE COLONNES DU TABLE
		String[] column_name = getNomColonnesTable(connect,nomTable);


		//OBTENTION DES NOMS DE FIELDS ET DES VALEURS DES FIELDS
		Field[] tabFields = get_fields(object);
		String[] fieldsName = FieldsName(tabFields);
		Vector<Object> fieldsValue = get_fieldsValue(object);
		
		String[] annoColonne = getColonnes(tabFields);

		//OBTENTION DES INDICES DE FIELDS A UTILISER
		Vector<Integer> indices_fields_concernes =  get_indices_fields_concernes(column_name, annoColonne);

		//CONSTRUCTION DE LA REQUETE INSERT
		String insert = construct_request_insert(nomTable, annoColonne, fieldsValue, indices_fields_concernes);
		System.out.println(insert);

		//EXECUTION DE LA REQUETE INSERT
		execute_query(connect, insert);


	}

	
	//getTable ne ferme pas la connection qu'elle utilise
	//les noms des colonnes sont conservees dans le dernier indice du vector
	public Vector<Object[]> getTable1(Connection connect, String nomTable, String filtre)throws SQLException{
		
		// RESSERVATION DE LA RESSOURCE
		Statement statement  =  connect.createStatement();
		Vector<Object[]> resultat = new Vector<Object[]>(5,5);	
		String q1 = "";
		try{
			//OBTENTION DES NOMS DES COLONNES
			int i = 0, u =0;
			if (filtre == null || filtre == ""){
				filtre = ""; 
			}
			else{
				filtre = " where " + filtre;
			}
			String query = "select * from " + nomTable + filtre;
			q1 = query;
			ResultSet resultset = statement.executeQuery(query);
			ResultSetMetaData rsmd = resultset.getMetaData();
	     	int nbrColumn = rsmd.getColumnCount();
			
			while(resultset.next() == true){
				Object[] temp_object = new Object[nbrColumn];
				
				for(i = 0; i < nbrColumn; i++){
					temp_object[i] = resultset.getObject(i+1);
				}
				resultat.add(temp_object);
			}
			String[] column_name = getNomColonnesTable(connect,nomTable);
			resultat.add( tabString_to_tabObject(column_name) );

		}
		catch(SQLException sqle){
			System.out.println(sqle.getMessage() + " " + q1);
		}
		finally{
			statement.close();
			return resultat;
		}
	}
		
	public static Vector<Object[]> select(Connection connect, String nomTable, String filtre)throws SQLException{
		
		// RESSERVATION DE LA RESSOURCE
		Statement statement  =  connect.createStatement();
		Vector<Object[]> resultat = new Vector<Object[]>(5,5);
		try{
			int i = 0, u = 0;
			if (filtre == null || filtre == ""){
				filtre = ""; 
			}
			else{
				filtre = " where " + filtre;
			}
			String query = "select * from " + nomTable + filtre;
			ResultSet resultset = statement.executeQuery(query);
			ResultSetMetaData rsmd = resultset.getMetaData();
	     	int nbrColumn = rsmd.getColumnCount();
			
			while(resultset.next() == true){
				//System.out.println("continue");
				Object[] temp_object = new Object[nbrColumn];
				
				for(i = 0; i < nbrColumn; i++){
					temp_object[i] = resultset.getObject(i+1);
				}
				resultat.add(temp_object);
			}
			//String[] column_name = getNomColonnesTable(connect,nomTable);
			//resultat.add( tabString_to_tabObject(column_name) );

		}
		catch(Exception e){
			try{
				String[] nomClass = nomTable.split("\\.");
				String query = "select * from " + nomClass[1] + filtre;
				ResultSet resultset = statement.executeQuery(query);
				ResultSetMetaData rsmd = resultset.getMetaData();
		     	int nbrColumn = rsmd.getColumnCount();
				
				while(resultset.next() == true){
					Object[] temp_object = new Object[nbrColumn];
					
					for(int i = 0; i < nbrColumn; i++){
						temp_object[i] = resultset.getObject(i+1);
					}
					resultat.add(temp_object);
					System.out.println("resultat_boucle = " + resultat.size());
				}
				String[] column_name = getNomColonnesTable(connect,nomTable);
				resultat.add( tabString_to_tabObject(column_name) );
			}
			catch(Exception e1){
				System.out.println("Nom de table incorrecte ou filtre mal construit.");
			}
		}
		finally{
			System.out.println("resultat = " + resultat.size());
			statement.close();
			return resultat;
		}
	}

	public Vector<String> supprDoublon(Vector<String> vect){
		int i = 0, u = 0;
		int NbrDoublon = 0;
		String compare, compared;
		for(i = 0; i < vect.size() -1; i++){
			compare = (String) vect.elementAt(i);
			for(u = i + 1; u < vect.size() - 1; u++){
				compared = (String) vect.elementAt(u);
				if(compare == compared){
					vect.remove(u);
				}
			}
		}
		return vect;
	}

	//get_fields, formulaire_getType et fieldsName sont utilisees pour la creation d'un formulaire
	public Field[] get_fields(java.lang.String nomClass)throws ClassNotFoundException{
		Field[] fields = Class.forName(nomClass).getDeclaredFields();;
		return fields;
	}

	public Double sumFieldvalue(Object[] tab, String fieldName)throws Exception{
		try{
			String mtdGet_name = construct_nameMtdGet(fieldName);
			Method mtdGet = tab[0].getClass().getMethod(mtdGet_name);//a appeler pour obtenir la valeur de l'attribut qu'on veut sommer
			//Field fieldToSum = tab[0].getClass().getDeclaredField(fieldName);
			//fieldToSum.setAccessible(true);
			//String fieldTypeName = fieldToSum.getType().getName();//le nom du type de retour
			//Class cl = Class.forName(fieldTypeName);
			//Object somme = cl.newInstance();
			Double somme_temp = 0.0;
			for(int i = 0; i < tab.length; i++){
				somme_temp += Double.parseDouble( mtdGet.invoke(tab[i]).toString() );
			}
			return somme_temp;
		}
		catch(Exception e){
			throw new Exception("sumFieldValue : " +e);
		}
	}

	//public String[] FieldsName(Field[] fields)
	public Vector<String> formulaire_getType(Field[] fields){
		int i = 0;
		Vector<String> type = new Vector<String>(5, 5);
		for(i = 0; i < fields.length; i++){
			Class cl = fields[i].getType();
			String nf = cl.getName();
			type.add(nf);
		}
		return type;
	}

	public static String[][] getResultSql(Connection connection ,String sql) throws Exception {
		Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
		try {
			ResultSet result = statement.executeQuery(sql);
			result.last();
			int nbR = result.getRow();
			ResultSetMetaData metadata = result.getMetaData();
			int nbC = metadata.getColumnCount();
			result.beforeFirst();
			String[][] tab = new String[nbR][nbC];
			int i = 0;
			while(result.next()) {
				for(int j=0;j<nbC;j++){
					tab[i][j] = result.getString(j+1);
				}
				i++;
			}
			
			return tab;
		} catch(SQLException e) {
			throw new Exception("Aucun resultat ou requete incorrecte !");
		} finally {
			statement.close();
		}
	}
}
