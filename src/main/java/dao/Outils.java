package dao;
import java.time.format.DateTimeFormatter;
import java.util.Vector;
import java.util.Date;
import java.time.LocalDateTime; //misy koa ny localDateTime mety mcontenir heure:minute:seconde
public class Outils{

    public Vector<Object[]> assembleVectorTabObject( Vector<Object[]> vect1, Vector<Object[]> vect2){
       Vector<Object[]> retour = new Vector<Object[]> (5,5);
        for(int i = 0; i < vect1.size(); i++){
            Object[] temp_obj = vect1.elementAt(i);
            retour.add(temp_obj);
        }
        for(int u = 0; u < vect2.size(); u++){
            Object[] temp_obj = vect2.elementAt(u);
            retour.add(temp_obj);
        }

        return retour;
    }

    
    public Object[] vectToTabObject(Vector<Object[]> vect, int indice){
        Object[] retour = (Object[]) vect.elementAt(indice);
        return retour;
    }

	static public String toUperCase0(String meth){
        return Character.toUpperCase(meth.toCharArray()[0])+meth.substring(1);
    }

    static public String toLowerCase0(String meth){
        return Character.toLowerCase(meth.toCharArray()[0])+meth.substring(1);
    }

    public String[] splitDayHour(String date){ // de format YYYYY-MON-DD HH:MN:SS
        String[] split = date.split("T");
        return split;
    }
    public String[] splitDayHour2(String date){ // de format YYYYY-MON-DD HH:MN:SS
        String[] split = date.split(" ");
        return split;
    }

    public String[] getYearMonthDay(String date){ // de format YYYYY-MON-DD HH:MN:SS
        String[] ymd= date.split("-");
        return ymd;
    }

    public String[] getHourMinuteSeconde(String hour){ // de format YYYYY-MON-DD HH:MN:SS
        String[] hms = hour.split(":");
        return hms;
    }
    public String verifSecond(String secondes){
        char[] seconde = new char[2];
        char[] char_seconde = secondes.toCharArray();
        for(int i = 0; i < 2; i++){
            seconde[i] = char_seconde[i];
        }
        String s = new String(seconde);
        return s;
    }

     public String stringDateToDate2(String date)throws Exception{
        String retour = "";
        try{
            String[] d = splitDayHour2(date);
            String[] day = getYearMonthDay(d[0]); //ato ny olana amin'le jour, mois , annee mifamadika
            String[] hour = getHourMinuteSeconde(d[1]);
            String seconde = verifSecond(hour[2]);
            retour = day[0] + "-" + day[1] + "-" + day[2] + " " + hour[0] + ":" + hour[1] + ":" + seconde;
        }
        catch(Exception e){
            throw new Exception("stringDateToDate2 : " + e);
        }
        return retour;
    }

    public String stringDateToDate(String date)throws Exception{
        String[] d = splitDayHour(date);
        String[] day = getYearMonthDay(d[0]); //ato ny olana amin'le jour, mois , annee mifamadika
        String[] hour = getHourMinuteSeconde(d[1]);
        System.out.println(date);
        String string_jour = new String(day[2]);
        int jour = stringToInt(string_jour);
        String retour = day[0] + "-" + day[1] + "-" + day[2] + "T" + hour[0] + ":" + hour[1] + ":" + hour[2];

        return retour;
    }
    public String dateToStringDate(String date)throws Exception{
        String[] d = splitDayHour(date);
        String[] day = getYearMonthDay(d[0]); //ato ny olana amin'le jour, mois , annee mifamadika
        String[] hour = getHourMinuteSeconde(d[1]);
        System.out.println(date);
        String string_jour = new String(day[2]);
        int jour = stringToInt(string_jour);
        String retour = day[0] + "-" + day[1] + "-" + day[2] + " " + hour[0] + ":" + hour[1];
        if(hour.length == 3){
            retour += ":" + hour[2];
        }
        else{
            retour += ":00";
        }

        return retour;
    }
    
    public String dateToStringDate(LocalDateTime date1)throws Exception{
        String date = date1.toString();
        String retour = dateToStringDate(date);
        return retour;
    }

    public String stringDateToDate3(String date)throws Exception{
        String[] d = splitDayHour(date);
        String[] day = getYearMonthDay(d[0]); //ato ny olana amin'le jour, mois , annee mifamadika
        String[] hour = getHourMinuteSeconde(d[1]);
        System.out.println(date);
        String string_jour = new String(day[2]);
        int jour = stringToInt(string_jour);
        String retour = day[0] + "-" + day[1] + "-" + day[2] + " " + hour[0] + ":" + hour[1] + ":" + "00";

        return retour;
    }

    public LocalDateTime stringDateToLocalDateTime(String date1)throws Exception{
        String[] d = splitDayHour(date1);
        String[] day = getYearMonthDay(d[0]);
        String[] hour = getHourMinuteSeconde(d[1]);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");
        LocalDateTime date = LocalDateTime.parse(date1, formatter);
        return date;
    }

    public void controlDate(LocalDateTime date)throws Exception{
    	int year = date.getYear();
    	int month = date.getMonthValue();
    	int day = date.getDayOfMonth();
    	int hour = date.getHour();
    	int minute = date.getMinute();
        int seconds = date.getSecond();
    	if(year < 2018){
    		throw new Exception("Annee invalide");
    	}
    	if(month < 1 || month > 12){
    		throw new Exception("Mois invalide");
    	}
    	if(day > getFinMois(month, year)){
    		throw new Exception("Jour superieur au fin du mois");
    	}
    	
        if(hour < 0 || hour > 23){
    		throw new Exception("Heure invalide");
    	}
    	if(minute < 0 || minute > 59){
    		throw new Exception("Minute invalide");
    	}
        if(seconds < 0 || seconds > 59){
            throw new Exception("Seconde invalide");
        }
    }

    public LocalDateTime getLocalDateTime(String date)throws Exception{//pour convertir une date venant de la base de donnees
        String string_date = stringDateToDate2(date);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime ldt_date = LocalDateTime.parse(string_date, formatter);

        return ldt_date;
    }

    //une fonction utile pour s'assurer qu'un mois ou un jour est composé de deux chiffres
    public String deuxChiffres(int nombre){
        String retour = Integer.toString(nombre);
        char[] nb = Integer.toString(nombre).toCharArray();
        if(nb.length < 2){
            char[] new_nb = new char[nb.length + 1];
            new_nb[0] = '0';
            new_nb[1] = nb[0];
            retour = new String(new_nb);
        }

        return retour;
    }

    public static int getFinMois(int month, int year) {
		if(month == 2 && year % 4 == 0) {
			return 29;
		}
		int[] mois = new int[]{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		
		return mois[month];
	}

    public int[] ajoutJour(int annee, int mois, int jour, int jourPlus){
        int i = 1;
        int finMois = getFinMois(mois, annee);
        jour += jourPlus;
        while(1 == 1){
            if(jour > finMois){
                jour = jour - finMois;
                if(mois == 12){
                    mois = 1;
                    annee++;
                }
                else{
                    mois++;
                }
            }
            else{
                break;
            }
            finMois = getFinMois(mois, annee);
            if(jour <= finMois){
                break;
            }
        }

        int[] date = new int[3];
        date[0] = annee;
        date[1] = mois;
        date[2] = jour;

        return date;
    }

    public LocalDateTime ajoutJour(LocalDateTime date0, int jourPlus)throws Exception{
        LocalDateTime datenull = getLocalDateTime("2017-01-01 00:00:00");
        try{
            String date = date0.toString();
            String[] d = splitDayHour(date);
            String[] day = getYearMonthDay(d[0]); //ato ny olana amin'le jour, mois , annee mifamadika
            String[] hour = getHourMinuteSeconde(d[1]);
            int annee = Integer.parseInt(day[0]);
            int mois = Integer.parseInt(day[1]);
            int jour = Integer.parseInt(day[2]);
            int[] jourfinal = ajoutJour(annee, mois, jour, jourPlus);
            String smois = deuxChiffres(jourfinal[1]);
            String sjour = deuxChiffres(jourfinal[2]);
            String string_date = jourfinal[0] + "-" + smois + "-" + sjour + " " + hour[0] + ":" + hour[1] + ":00";
            LocalDateTime ldt_date = getLocalDateTime(string_date);

            return ldt_date;
        }
        catch(Exception e){
            System.out.println("ajoutJour : " + e);
        }
        return datenull;
    }

    public int differenceJour(int anneeDepart, int moisDepart, int jourDepart, int anneeArrive, int moisArrive, int jourArrive){
        Date debut = new Date(anneeDepart, moisDepart, jourDepart, 0, 0);
        Date fin = new Date(anneeArrive, moisArrive, jourArrive, 0, 0);
        long long_diffJour = (fin.getTime() - debut.getTime()) / 1000 / 60 / 60 /24;
        String string_diffJour = Long.toString(long_diffJour);
        int int_diffJour = Integer.parseInt(string_diffJour);
        //System.out.println(long_diffJour);

        return int_diffJour;
    }

    public boolean jourIdentique(int anneeDepart, int moisDepart, int jourDepart, int anneeArrive, int moisArrive, int jourArrive){
        if(anneeDepart != anneeArrive){
            return false;
        }
        if(moisDepart != moisArrive){
            return false;
        }
        if(jourDepart != jourArrive){
            return false;
        }
        return true;
    }

    public int getInt(char chiffre)throws Exception{
        int retour = 10;
        char[] chiffres = new char[10];
        chiffres[0] = '0';
        chiffres[1] = '1';
        chiffres[2] = '2';
        chiffres[3] = '3';
        chiffres[4] = '4';
        chiffres[5] = '5';
        chiffres[6] = '6';
        chiffres[7] = '7';
        chiffres[8] = '8';
        chiffres[9] = '9';
        for(int i = 0 ; i < chiffres.length; i++){
            if(chiffre == chiffres[i]){
                retour = i;
            }
        }
        if(retour == 10){
            throw new Exception("Impossible de chiffrer le caractere " + chiffre);
        }

        return retour;
    }

    public int get_dixPuissance(int p){ // p est l'exposant de 10 ou 10^p
        int dixPuissance = 1;
        for(int i = 1; i <= p; i++){
            dixPuissance = dixPuissance * 10;
        }

        return dixPuissance;
    }

    public int reconstruct_number(int[] nombre){
        int retour = 0;
        int u = nombre.length - 1; // u est l'indice du chiffre multiplie par 10^0
        for(int i = 0; i < nombre.length; i++){
            retour += nombre[u] * get_dixPuissance(i);
            u--; 
        }

        return retour;
    }

    public int stringToInt(String nombre)throws Exception{
        char[] char_number = nombre.toCharArray();
        int[] int_number = new int[char_number.length];
        for(int i = 0; i < char_number.length; i++){
            int_number[i] = getInt(char_number[i]);
        }
        int nombre_reconstitue = reconstruct_number(int_number);

        return nombre_reconstitue;
    }

    public String[] reduireTable(String[] tab, int taille){
        String [] tab_reduit = new String[taille];
        for(int i = 0; i < taille; i++){
            tab_reduit[i] = tab[i];
        }

        return tab_reduit;
    }
    public void arrondirNombre(float nombre){
        String string_nombre = Float.toString(nombre);
        char[] chiffres = string_nombre.toCharArray();
        System.out.println(new String(chiffres));
    }

    
      
    public static void main(String[] args)throws Exception{
        Outils outils = new Outils();
        /*String haha = "2018-02-05 00:00:11.2";
        haha = outils.stringDateToDate2(haha);
        System.out.println(haha);
        */
        float b = 4;
        outils.arrondirNombre(b);
    }

}