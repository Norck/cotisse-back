package app;
public class HttpResponse {
    public int status;
    public String message;
    public Object[] data;

    public HttpResponse(int status, String message, Object[] data){
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
