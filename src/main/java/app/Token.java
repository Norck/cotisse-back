package app;

import dao.*;
import org.apache.tomcat.jni.Local;
import voyage.Login;
import voyage.Utilisateur;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

@Table(name="token")
public class Token {

    @PrimaryKey(beforeSeq = "tk", sequenceName = "seq_token")
    @Colonne(colonne = "id")
    private String id;

    @Colonne(colonne = "token")
    private String token;

    @Colonne(colonne = "idUtilisateur")
    private String idUtilisateur;

    @Colonne(colonne = "dateCreation")
    private LocalDateTime dateCreation;

    @Colonne(colonne = "dateExpiration")
    private LocalDateTime dateExpiration;

    @Colonne(colonne="dateFin")
    private LocalDateTime dateFin;

    @Colonne(colonne="etat")
    private int etat;

    public Token(){}

    public LocalDateTime getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDateTime dateFin) {
        this.dateFin = dateFin;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public HttpResponse unvalidateToken() {
        Connection connection = null;
        HttpResponse response = null;
        try {
            connection = Connect.connect("src/main/java/dao/postgresql.xml");
            String query = "update token set etat=0, dateFin='" + new Outils().dateToStringDate(LocalDateTime.now()) + "' where token='" + token + "'";
            new OutilsSql().execute_query(connection, query);
            response = new HttpResponse(203, "ok", null);
        }catch(Exception e){
            response = new HttpResponse(203, "erreur", new Object[]{e});
        }finally{
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return response;
        }
    }

    public Token(String id, String token, String idUtilisateur, LocalDateTime dateCreation, LocalDateTime dateExpiration, LocalDateTime dateFin, int etat) {
        this.id = id;
        this.token = token;
        this.idUtilisateur = idUtilisateur;
        this.dateCreation = dateCreation;
        this.dateExpiration = dateExpiration;
        this.dateFin = dateFin;
        this.etat = etat;
    }

    public HttpResponse login(Login login){
        Connection connection = null;
        GenericDAO dao = new GenericDAO(5, "src/main/java/dao/postgresql.xml");
        HttpResponse response = null;
        try{
            connection = Connect.connect("src/main/java/dao/postgresql.xml");
            connection.setAutoCommit(false);
            Utilisateur util = new Utilisateur(null, login.nom, login.mdp, null, null);
            String[] fields = new String[]{"nom", "mdp"};
            String[][] result = new OutilsSql().getResultSql(connection, "select * from utilisateur where nom='" + login.nom + "' and mdp=md5('" + login.mdp + "')");
            if(result.length != 0){
                util = new Utilisateur(result[0][0], result[0][1], result[0][2], result[0][3], result[0][4]);
                LocalDateTime dateCreation = LocalDateTime.now();
                LocalDateTime dateExpiration = dateCreation.plusDays(1);
                Token token = new Token(null, generateToken(50), util.getId(), dateCreation, dateExpiration, dateExpiration, 1);
                dao.insert(connection, token);
                connection.commit();
                response = new HttpResponse(203, "Logged", new Object[]{token});
            }
            else{
                response = new HttpResponse(203, "Nom ou mot de passe incorrect", null);
            }
        } catch (Exception e) {
            response = new HttpResponse(203, "Une erreur inconnue s'est produite", new Object[]{e});
        }finally{
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return response;
        }
    }

    //retourne null si le token est invalide
    public Token getTokenDetails(Connection connection, String token) throws Exception {
        GenericDAO dao = new GenericDAO(5, "src/main/java/dao/postgresql.xml");
        Token tokenObj = new Token((String)null, token, (String)null, (LocalDateTime)null, (LocalDateTime)null, (LocalDateTime)null, 0);
        Object var5 = null;

        try {
            String[] field = new String[]{"token"};
            Object[] result = dao.find(connection, tokenObj, field, 1);
            Token temp = (Token)result[0];
            System.out.println(temp.getToken());
            return temp;
        } catch (Exception var9) {
            throw var9;
        }
    }

    public String generateToken(int nbChar){
        String token = null;
        String model = "qwCVBNM123456ertyuiopa/!@#$%^GHJKLZX7890&*()_+{RTYUIOPASD}:\"<>?sdfghjklzxcvbnmQWEF-=[];,.\\";
        char[] tabChar = model.toCharArray();
        char[] char_token = new char[nbChar];
        for(int i = 0; i < nbChar; i++){
            char_token[i] = tabChar[new Random().nextInt(nbChar)];
        }
        token = new String(char_token);
        return token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(String idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public LocalDateTime getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(LocalDateTime dateExpiration) {
        this.dateExpiration = dateExpiration;
    }
}
