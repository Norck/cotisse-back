package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import voyage.*;

import java.util.Map;

@SpringBootApplication
@RestController
@EnableAutoConfiguration
public class Application {

	@GetMapping("/hello/{name}/{num}")
	public String hello(@PathVariable String name, @PathVariable int num){
		return "Hello " + name + " , num = " + num;
	}

	@GetMapping("/")
	public String hello(){
		return "Hello";
	}

	@CrossOrigin
	@PostMapping("/insertVoyage")
	HttpResponse insertVoyage(@RequestBody Voyage voyage){
		System.out.println(voyage.getDateVoyage());
		return voyage.insertVoyage();
	}

	@CrossOrigin
	@PostMapping("/validerVoyage")
	HttpResponse validerVoyage(@RequestBody VoyageConfirme voyage){
		HttpResponse response = voyage.validerVoyage();
		return response;
	}

	@CrossOrigin
	@PostMapping("/annulerVoyage")
	public HttpResponse annulerVoyage(@RequestBody VoyageAnnule voyageAnnule){
		HttpResponse response = voyageAnnule.annulerVoyage();
		return response;
	}

	@CrossOrigin
	@PostMapping("/setHeureArrive")
	public HttpResponse setHeureArrive(@RequestBody ArriveVoyage etat){
		HttpResponse response = etat.setHeureArrive();
		return response;
	}

	@CrossOrigin
	@PostMapping("/confirmerArriveeVoyage")
	public HttpResponse confirmerArriveeVoyage(@RequestBody ConfirmationArriveVoyage arrive){
		HttpResponse response = arrive.confirmerArriveVoyage();
		return response;
	}

	@CrossOrigin
	@GetMapping("/user")
	@ResponseStatus(value= HttpStatus.OK)
	public String all(@RequestHeader(value="nom") String user){
		return "user.name" + user;
	}

	@GetMapping("/log")
	@ResponseStatus(value = HttpStatus.OK)
	public void all(@RequestHeader Map<String, String> headers, @RequestBody String message){
		System.out.println("nom = " + headers.get("nom"));
		System.out.println("pass = " + headers.get("pass"));
		System.out.println("message = " + message);
	}

	@CrossOrigin
	@GetMapping("/sendHeaders")
	public HttpResponse sendHeaders(){
		HttpHeaders headers = new HttpHeaders();
		headers.set("afatra", "mbola misy @ heritaona");
		return new HttpResponse(200, "ok", null);
	}

	@CrossOrigin
	@PostMapping("/login")
	public HttpResponse login(@RequestBody Login login){
		HttpResponse response = new Token().login(login);
		return response;
	}

	@CrossOrigin
	@PostMapping("/unvalidateToken")
	public HttpResponse unvalidateToken(@RequestBody Token token){
		HttpResponse response = token.unvalidateToken();
		return response;
	}



	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
